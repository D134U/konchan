from django.db import models

# Здесь будут записи


class Thread(models.Model):
    author = models.CharField('Автор', max_length=99)
    title = models.CharField('Заголовок', max_length=500)
    text = models.TextField('Textfield')
    date = models.DateField('Date')

    def __str__(self):
        return self.title

