from .models import Thread
from django.forms import ModelForm


class ThreadForm(ModelForm):
    class Meta:
        model = Thread
        fields = ['title', 'text']
