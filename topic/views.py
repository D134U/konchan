from django.shortcuts import render
from .models import Thread
from .forms import ThreadForm

# Create your views here.


def dashboard(request):
    threads = Thread.objects.all()
    return render(request, 'topic/dashboard.html')


def create(request):
    if request.method == 'POST':
        form = ThreadForm(request.POST)

    if form.is_valid():
        form.save()
    else:
        pass

    form = ThreadForm
    data = {
        'form': form
     }
    return render(request, 'topic/create_topic.html', data)
